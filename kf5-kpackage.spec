%undefine __cmake_in_source_build
%global framework kpackage

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:           kf5-%{framework}
Version:        5.116.0
Release:        1
Summary:        KDE Frameworks 5 Tier 2 library to load and install packages as plugins

License:        CC0-1.0 AND GPL-2.0-or-later AND LGPL-2.0-or-later
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5

Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

## upstream patches

BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-rpm-macros
BuildRequires:  kf5-karchive-devel >= %{majmin}
BuildRequires:  kf5-kcoreaddons-devel >= %{majmin}
BuildRequires:  kf5-ki18n-devel >= %{majmin}
BuildRequires:  kf5-kdoctools-devel >= %{majmin}
BuildRequires:  qt5-qtbase-devel
BuildRequires:  make

%description
KDE Frameworks 5 Tier 2 library to load and install non-binary packages as
if they were plugins.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       extra-cmake-modules >= %{majmin}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version} -p1

%build
%{cmake_kf5} \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}

%cmake_build

%install
%cmake_install

%find_lang %{name} --all-name --with-man

# create/own dirs
mkdir -p %{buildroot}%{_kf5_qtplugindir}/kpackage/packagestructure/
mkdir -p %{buildroot}%{_kf5_datadir}/kpackage/

%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
make test ARGS="--output-on-failure --timeout 30" -C %{_target_platform} ||:
%endif

%ldconfig_scriptlets

%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf5_datadir}/qlogging-categories5/%{framework}.*
%{_kf5_libdir}/libKF5Package.so.*
%{_kf5_qtplugindir}/kpackage/
%{_kf5_datadir}/kpackage/
%{_kf5_datadir}/kservicetypes5/kpackage-*.desktop
%{_kf5_bindir}/kpackagetool5
%{_mandir}/man1/kpackagetool5.1*

%files devel
%{_kf5_includedir}/KPackage/
%{_kf5_libdir}/libKF5Package.so
%{_kf5_libdir}/cmake/KF5Package/


%changelog
* Mon Nov 25 2024 peijiankang <peijiankang@kylinos.cn> - 5.116.0-1
- Upgrade package to 5.116.0

* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.115.0-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 04 2024 peijiankang <peijiankang@kylinos.cn> - 5.115.0-1
- update verison to 5.115.0

* Mon Jan 08 2024 peijiankang <peijiankang@kylinos.cn> - 5.113.0-1
- update verison to 5.113.0

* Thu Aug 03 2023 haomimi <haomimi@uniontech.com> - 5.108.0-1
- update verison to 5.108.0

* Mon Dec 12 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 5.100.0-1
- Update to upstream version 5.100.0

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.97.0-1
- update to upstream version 5.97.0

* Tue Jul 05 2022 tanyulong <tanyulong@kylinos.cn> - 5.95.0-1
- update to upstream version 5.95.0

* Sat Feb 12 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.90.0-1
- update to upstream version 5.90.0

* Sun Jan 16 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.88.0-1
- update to upstream version 5.88.0

* Mon Aug 17 2020 yeqinglong <yeqinglong@uniontech.com> - 5.55.0-1
- Initial release for OpenEuler
